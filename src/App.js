import React from 'react';
import logo from './logo.svg';
import { IoIosImage } from "react-icons/io";
import './App.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toDoItems: [],
      completedItems: []
    }
  }

  componentDidMount () {
    document.getElementById("noteInput").addEventListener("keyup", function(event) {
      if (event.key === "Enter") {
        this.onAddItem();
      }
  }.bind(this));
  }

  onImageIconClick = (evt) => {
    console.log('imageIconClick');
    document.getElementById("imageInput").click()
  }

  onAddItem = (evt) => {
    let inputValue = document.getElementById("noteInput").value;
    document.getElementById("noteInput").value = "";
    console.log('additem');
    if (inputValue.length === 0) {
      return;
    }
    // debugger;

    let toDoItems = this.state.toDoItems;
    toDoItems.push(inputValue);
    this.setState({
      toDoItems
    })

  }

  onClickToDoItem = (evt,i) => {
    // debugger;
    let toDoItems = this.state.toDoItems; 
    let completedItems = this.state.completedItems; 
    let itm = toDoItems[i];
    evt.target.checked = false;
    toDoItems.splice(i,1);
    completedItems.push(itm);
    this.setState({
      toDoItems,
      completedItems
    })
  }



  render() {
    let todoItems = this.state.toDoItems;
    let completedItems = this.state.completedItems;
    return (
      <div >

        <div>
          <h2>To Do App</h2>
        </div>
        <div className="App">
          <input className="NoteInputCss" id="noteInput" placeholder="Take a note..."></input>
          <span className="imageIconCss" onClick={this.onImageIconClick}>
            <input type="file" name="image" id="imageInput" className="fileInput" />
            <IoIosImage />
          </span>

          <button onClick={this.onAddItem}>Add Item</button>
        </div>

        <div>
          <h3>To Do Items</h3>
          {todoItems.map((itm, i) => {
            return (
              <div id={itm + i}>
                <input type="checkbox" onClick={(e) => this.onClickToDoItem(e,i)} />
                <label>{itm}</label>
              </div>
            )
          })}
        </div>
        <div>
          <h3>Completed Items</h3>
          {completedItems.map((itm) => {
            return (
              <div>
                <label>{itm}</label>
              </div>
            )
          })}
        </div>

      </div>
    )
  }

}
